

public class Question {

    public String text;  // storing questions
    public String[] answers; // storing answers
    public String rightAnswer; // storing right answer

    public Question(String text, String... answers) {
        this.text = text;
        this.answers = answers;
        this.rightAnswer = answers[0]; // küsimuste esimene[0] vastus on õige

        for (int i = 0; i < answers.length; i++) {
            int randomIndex = (int) (Math.random() * answers.length);
            String tmp = answers[i];
            answers[i] = answers[randomIndex];
            answers[randomIndex] = tmp;

        }


    }

    public Question() {

    }



}
