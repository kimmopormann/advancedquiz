

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Game {

    // For storing all the question in one ArrayList
    private static ArrayList<Question> questions = new ArrayList<Question>();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {


        System.out.println("1\t Play Capital Quiz ");
        System.out.println("2\t Play Name Quiz ");
        System.out.println("3\t Add new question to Capital quiz ");
        System.out.println("4\t Add new question to Name quiz " + "\n");
        System.out.println("Make your choice?");
        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
                init();
                loop();
                break;
            case 2:
                init2();
                loop();
                break;
            case 3:
                addNewQuestionToCapitalQuiz();
                break;
            case 4:
                addNewQuestionToNameQuiz();
                break;
        }

    }


    // Initialize capital game
    private static void init() {

        try {
            List<String> lines = Files.readAllLines(Paths.get("capitalquiz.txt"));

            for (int i = 0; i < lines.size(); i += 4) {
                Question q;
                q = new Question(lines.get(i), lines.get(i + 1), lines.get(i + 2), lines.get(i + 3));
                questions.add(q);
            }


        } catch (Exception e) {
            System.out.println(e);
            System.out.println("Could not load file");
            System.exit(-1);
        }

    }

    // Initialize name game
    private static void init2() {
        try {
            List<String> lines = Files.readAllLines(Paths.get("namequiz.txt"));

            for (int i = 0; i < lines.size(); i += 5) {
                Question q;
                q = new Question(lines.get(i), lines.get(i + 1), lines.get(i + 2), lines.get(i + 3), lines.get(i + 4));
                questions.add(q);
            }


        } catch (Exception e) {
            System.out.println(e);
            System.out.println("Could not load file");
            System.exit(-1);
        }

    }

    // Game play loop
    private static void loop() {

        int score = 0;

        while (questions.size() > 0) {
            Question currentQuestion = questions.remove(0);

            //Printing question
            System.out.println(currentQuestion.text);

            //Printing all answers
            for (int i = 0; i < currentQuestion.answers.length; i++) {
                System.out.println(i + 1 + ") " + currentQuestion.answers[i]);
            }

            //Asking for right answer
            System.out.println("Answer :");
            int input = scanner.nextInt();

            //Validating input from user
            if (input < 0 || input > currentQuestion.answers.length) {
                System.out.println("Input is invalid");
                System.exit(-2);
            }

            //Checking if answer is correct
            if (currentQuestion.rightAnswer.equals(currentQuestion.answers[input - 1])) {
                System.out.println("Right");
                score++;
            } else {
                System.out.println("Wrong");
            }


        }

        System.out.println("You got " + score + " questions right");

    }

    private static void addNewQuestionToCapitalQuiz() {

        try {
            File file = new File("capitalquiz.txt");
            if(!file.exists()){
                file.createNewFile();
            }

            System.out.println("Adding new question to the quiz!");
            String newQuestion = scanner.nextLine();
            System.out.println("Enter new question: ");
            String answer1 = scanner.nextLine();
            System.out.println("Enter answer 1");
            String answer2 = scanner.nextLine();
            System.out.println("Enter answer 2");
            String answer3 = scanner.nextLine();
            System.out.println("Enter answer 3");
            String answer4 = scanner.nextLine();

            FileWriter fw = new FileWriter(file,true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            //This will add a new line to the file content
            // pw.println("");
            pw.println("");
            pw.println(answer1);
            pw.println(answer2);
            pw.println(answer3);
            pw.println(answer4);
            pw.close();



           
        } catch(IOException ioe) {
            System.out.println("Something went wrong");
            ioe.printStackTrace();

        }
    }


    private static void addNewQuestionToNameQuiz() {
        try {
            File file = new File("namequiz.txt");
            if(!file.exists()){
                file.createNewFile();
            }

            System.out.println("Adding new question to the quiz!");
            String newQuestion = scanner.nextLine();
            System.out.println("Enter new question: ");
            String answer1 = scanner.nextLine();
            System.out.println("Enter answer 1");
            String answer2 = scanner.nextLine();
            System.out.println("Enter answer 2");
            String answer3 = scanner.nextLine();
            System.out.println("Enter answer 3");
            String answer4 = scanner.nextLine();
            System.out.println("Enter answer 4");
            String answer5 = scanner.nextLine();

            FileWriter fw = new FileWriter(file,true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);

            pw.println("");
            pw.println(answer1);
            pw.println(answer2);
            pw.println(answer3);
            pw.println(answer4);
            pw.println(answer5);
            pw.close();


        } catch(IOException ioe) {
            System.out.println("Something went wrong");
            ioe.printStackTrace();
        }

    }
}