import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileExample {
    public static void main(String[] args) throws IOException {

        // custom error messages
        List<String> lines = Files.readAllLines(Paths.get("capitalquiz.txt"));
        List<String> lines2 = Files.readAllLines(Paths.get("namequiz.txt"));

        System.out.println(lines);



    }
}
